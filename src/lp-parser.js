export function parser(input) {
  let inputTokenized = input;

  // Check if the input is already tokenized. 
  if ( !Array.isArray(inputTokenized) )
    inputTokenized = lexer(input);

  // If the length is 1, then we have an atomic formula. Then, it must be a
  // proposition symbol.
  if ( inputTokenized.length === 1 ) {
    if ( inputTokenized.at(0) instanceof PropositonSymbol ) {
      return inputTokenized[0];
    } else {
      throw new SyntaxError(`The string has lenght 1, but it isn't an atomic formula.`);
    }
  // If the lenght is greater thank 1, thne we have a complex formula. We have
  // to look for its head and parse its children.
  } else {
    // Find the index of the head.
    const currentHeadIndex = headIndex(inputTokenized);

    // If the head is an unary connective, then parse its only child.
    if ( inputTokenized[currentHeadIndex] instanceof UnaryConnectiveSymbol ) {
      const child = inputTokenized.slice(currentHeadIndex + 1, -1);
      inputTokenized[currentHeadIndex].children.push(parser(child));

      return inputTokenized[currentHeadIndex];

    // If the head is a binary connective, then parse its two children.
    } else if ( inputTokenized[currentHeadIndex] instanceof BinaryConnectiveSymbol ) {
      const leftChild = inputTokenized.slice(1, currentHeadIndex);
      inputTokenized[currentHeadIndex].children.push(parser(leftChild));

      const rightChild = inputTokenized.slice(currentHeadIndex + 1, -1);
      inputTokenized[currentHeadIndex].children.push(parser(rightChild));

      return inputTokenized[currentHeadIndex];
    
    // If it's neither, then we have an unknown connective.
    } else {
      throw new SyntaxError(`Ops, something went wrong.`);
    }
  }
}

function headIndex(input) {
  if ( !Array.isArray(input) ) {
    throw new Error(`The input for the depth function must be a tokenized string.`);
  }

  const initialSegment = [];

  for (let [index, value] of input.entries()) {
    initialSegment.push(value);

    if ( depth(initialSegment) === 1 && initialSegment.at(-1) instanceof ConnectiveSymbol ) {
      return index;
    }
  }

  throw new SyntaxError(`The string has lenght > 1, but it doesn't have an identifiable head.`)
}

function depth(input) {

  if ( !Array.isArray(input) ) {
    throw new Error(`The input for the depth function must be a tokenized string.`);
  }

  let depth = 0;

  for (let c of input) {
    if ( c instanceof LeftParenthesisSymbol )
      depth++;
      
    if ( c instanceof RightParenthesisSymbol )
      depth--;
  }

  return depth;
}

function lexer(input) {
  const inputTokenized = [];

  if (typeof input !== "string") {
    return;
  }

  for (let c of input) {
    if (PropositonSymbol.isMatch(c)) {
      inputTokenized.push(new PropositonSymbol(c));
    } else if (NegationSymbol.isMatch(c)) {
      inputTokenized.push(new NegationSymbol(c));
    } else if (ConjunctionSymbol.isMatch(c)) {
      inputTokenized.push(new ConjunctionSymbol(c));
    } else if (DisjunctionSymbol.isMatch(c)) {
      inputTokenized.push(new DisjunctionSymbol(c));
    } else if (ConditionalSymbol.isMatch(c)) {
      inputTokenized.push(new ConditionalSymbol(c));
    } else if (BiconditionalSymbol.isMatch(c)) {
      inputTokenized.push(new BiconditionalSymbol(c));
    } else if (LeftParenthesisSymbol.isMatch(c)) {
      inputTokenized.push(new LeftParenthesisSymbol(c));
    } else if (RightParenthesisSymbol.isMatch(c)) {
      inputTokenized.push(new RightParenthesisSymbol(c));
    } else if (c === " ") {
      // skip if it's a space
    } else {
      throw new SyntaxError(`The symbol "${c}" is not part of the lexicon.`);
    }
  }

  return inputTokenized;
}

class LPSymbol {
  constructor(symbol) {
    this.symbol = symbol;
  }

  static isMatch(input) {
    if ( this.symbols.includes(input) ) {
      return true;
    } else {
      return false;
    }
  }
}

class ConnectiveSymbol extends LPSymbol {
  constructor(symbol) {
    super(symbol);
    this.children = [];
  }
}

class PropositonSymbol extends LPSymbol {
  static symbols = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
}

class UnaryConnectiveSymbol extends ConnectiveSymbol {
}

class BinaryConnectiveSymbol extends ConnectiveSymbol {
}

class NegationSymbol extends UnaryConnectiveSymbol {
  static symbols = ["¬"]
}

class ConjunctionSymbol extends BinaryConnectiveSymbol {
  static symbols = ["∧"]
}

class DisjunctionSymbol extends BinaryConnectiveSymbol {
  static symbols = ["∨"]
}

class ConditionalSymbol extends BinaryConnectiveSymbol {
  static symbols = ["→"]
}

class BiconditionalSymbol extends BinaryConnectiveSymbol {
  static symbols = ["↔"]
}

class LeftParenthesisSymbol extends LPSymbol {
  static symbols = ["("]
}

class RightParenthesisSymbol extends LPSymbol {
  static symbols = [")"]
}

class SyntaxError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Syntax Error';
  }
}
