import React from 'react';

const Node = ({data}) => {
  return (
    <div className="Node">
      <div>{data.symbol}</div>
      { data.children && <div className="Children"> { data.children.map( child => <Node data={child} key={`component-${Math.random().toString(16).slice(2)}`} /> ) } </div> }
    </div>
   );
}

export default Node;
