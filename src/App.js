import { useEffect, useRef, useState } from "react";
import { parser } from "./lp-parser";
import Tree from "./Tree/Tree";

function App() {
  const formulaInputElement = useRef(null);
  const [formulaInput, setFormulaInput] = useState("");
  const [caretPosition, setCaretPosition] = useState(-1);
  const [parserResult, setParserResult] = useState([]);
  const [parserError, setParserError] = useState("");

  useEffect(() => {
    formulaInputElement.current.selectionStart = caretPosition;
    formulaInputElement.current.selectionEnd = caretPosition;

    setParserError("");
    setParserResult([]);

    try {
      formulaInput.length && setParserResult(parser(formulaInput));
    } catch (error) {
      setParserError(error);
      setParserResult([]);
    }
  }, [formulaInput, caretPosition]);

  const onFormulaInputChange = (event) => {
    if (event.target.value.includes("~")) {
      setCaretPosition(event.target.selectionStart);
      setFormulaInput(event.target.value.replace("~", "¬"));
    } else if (event.target.value.includes("<->")) {
      setCaretPosition(event.target.selectionStart - 2);
      setFormulaInput(event.target.value.replace("<->", "↔"));
    } else if (event.target.value.includes("->")) {
      setCaretPosition(event.target.selectionStart - 1);
      setFormulaInput(event.target.value.replace("->", "→"));
    } else if (event.target.value.includes("\\/")) {
      setCaretPosition(event.target.selectionStart - 1);
      setFormulaInput(event.target.value.replace("\\/", "∨"));
    } else if (event.target.value.includes("/\\")) {
      setCaretPosition(event.target.selectionStart - 1);
      setFormulaInput(event.target.value.replace("/\\", "∧"));
    } else {
      setCaretPosition(event.target.selectionStart);
      setFormulaInput(event.target.value);
    }
  };

  return (
    <div className="container my-5">
      <div className="row justify-content-center">
        <div className="col-12 text-center">
          <h1 className="mb-5">Language of Propositions Parser</h1>
          <div className="mb-3">
            <input
              id="formulaInput"
              className="form-control text-center mb-3"
              aria-label="Formula"
              aria-describedby="formulaInputHelp"
              onChange={onFormulaInputChange}
              value={formulaInput}
              ref={formulaInputElement}
            ></input>
            <div id="formulaInputHelp" className="form-text mb-3">
              Input the formula to be parsed! You can use "/ \" for
              conjunctions, "\ /" for disjunctions, "-&gt;" for conditionals,
              "&lt;-&gt;" for biconditionals and "~" for negations.
            </div>
          </div>
          {!parserError ? (
            <div className="py-5">
              {parserResult ? <Tree data={parserResult} /> : ""}
            </div>
          ) : (
            <div className="alert alert-danger my-5" role="alert">
              {parserError.toString()}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
