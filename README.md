# LP-Parser

LP-Parser is a simple parser for propositional logic with a typical infix
notation built with JavaScript and React. It parses the following grammar:

```ebnf
<formula>       ::= <proposition> | <negation> | <conjunction> | <disjunction> | <conditional> | <biconditional>
<negation>      ::= "(" "¬" <formula> ")"
<conjunction>   ::= "(" <formula> "∧" <formula> ")"
<disjunction>   ::= "(" <formula> "∨" <formula> ")"
<conditional>   ::= "(" <formula> "→" <formula> ")"
<biconditional> ::= "(" <formula> "↔" <formula> ")"
<proposition>   ::= [A-Z]
```

You can chek it out in action here:

https://writeonlycode.gitlab.io/lp-parser/

![Screenshot](public/image.png)

## How it Works Behind the Scenes 

LP-Parser comprises a lexer, a parser, a few utility functions to find the
depth of each symbol and the head of each formula, and a class structure to
represent tokens/nodes of each symbol in the language.

The lexer just go through the input string and returns an array of objects that
corresponds to the types of symbols in the input string: `PropositionSymbol`,
`NegationSymbol`, `ConjunctionSymbol`, `DisjunctionSymbol`,
`ConjunctionSymbol`, `BiconditionalSymbol`, `NegationSymbol`,
`LeftParenthesisSymbol`, and `RightParenthesisSymbol`. It skips spaces, but if
it find other symbols that aren't in the grammar, it throws an `SyntaxError`.

The parser is a recursive grammar driven parser: it search for the head of the
formula, isolates its subformula, passes it to be parsed, and push the result
to its list of children.

## Installation and Deployment

This project was bootstrapped with [Create React
App](https://github.com/facebook/create-react-app). In the project directory,
you can run:

### `npm start`

Runs the app in the development mode. Open
[http://localhost:3000](http://localhost:3000) to view it in your browser. The
page will reload when you make changes. You may also see any lint errors in the
console.

### `npm run build`

Builds the app for production to the `build` folder. It correctly bundles React
in production mode and optimizes the build for the best performance. The build
is minified and the filenames include the hashes. Your app is ready to be
deployed! See the section about
[deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.

